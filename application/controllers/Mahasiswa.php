<?php
class Mahasiswa extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('Model_mahasiswa', 'mhs');
    }

    function index(){
        if (!isset($this->session->username)) {
            $this->load->view('view_login');
        } else {
            $data = $this->mhs->read();
            $this->load->view('view_dashboard', array('data' => $data));
        }
    }

    function insert_mahasiswa(){

        $nama = $this->input->post('nama');
        $nim = $this->input->post('nim');

        $data = array(
            'nama' => $nama,
            'nim' => $nim
        );

        $cek = $this->mhs->insert($data);
        if ($cek > 0) {
            echo "<script type='text/javascript'>alert('Berhasil Input');</script>";
            // redirect(base_url('index.php/Mahasiswa?m=sukses'));
            $this->index();
        }
        
    }

    function delete_mahasiswa()
    {
        $id = $this->input->get('id');
        $cek = $this->mhs->delete($id);

        if ($cek > 0) {
            echo "<script type='text/javascript'>alert('Berhasil Hapus');</script>";
            // redirect(base_url('index.php/Mahasiswa?m=sukses'));
            $this->index();
        }
    }

    function update_mahasiswa()
    {
        $id = $this->input->get('id');

        $data = $this->mhs->getDataById($id);

        $this->load->view('view_formedit', array('data' => $data));
    }

    function update_proses()
    {
        //menerima data dari view_formedit.php
        $id = $this->input->post('id');
        $nama = $this->input->post('nama');
        $nim = $this->input->post('nim');

        $data = array(
            'id' => $id,
            'nama' => $nama,
            'nim' => $nim
        );

        $cek = $this->mhs->update($id, $data);
        if ($cek > 0) {
            echo "<script type='text/javascript'>alert('Berhasil Edit');</script>";

            $this->index();
        } else {
            echo "<script type='text/javascript'>alert('Gagal Edit');</script>";
            $this->index();
        }
    }

    function login()
    {

        $data['nama'] = $this->input->post('username');
        $data['nim'] = $this->input->post('password');

        $cek = $this->mhs->getLogin($data);

        if (isset($cek)) {
            if ($cek->nama == $data['nama'] && $cek->nim == $data['nim']) {

                $data_session = array(
                    'username' => $data['nama'],
                    'login_status' => TRUE
                );

                $this->session->set_userdata($data_session);

                $this->index();
            }
        } else {
            $this->index();
        }
    }

    function logout()
    {
        // session_destroy();
        unset($_SESSION['username']);
        unset($_SESSION['login_status']);

        $this->index();
    }
}
