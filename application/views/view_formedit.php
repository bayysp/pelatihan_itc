<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Form Edit</title>
</head>

<body>
    <center>
        <form action="<?php echo base_url('index.php/Mahasiswa/update_proses'); ?>" method="POST">
            <table>
            <?php foreach($data as $mhs){ ?>
                <tr>
                    <td>ID</td>
                    <td>:</td>
                    <td><input type="text" name="id" value="<?php echo $mhs->id; ?>" readonly></td>
                </tr>
                <tr>
                    <td>Nama</td>
                    <td>:</td>
                    <td><input type="text" name="nama" value="<?php echo $mhs->nama; ?>" ></td>
                </tr>
                <tr>
                    <td>Nim</td>
                    <td>:</td>
                    <td><input type="text" name="nim" value="<?php echo $mhs->nim; ?>"></td>
                </tr>
            </table>
            <?php } ?>
            <input type="submit" value="Edit">
            <a href="<?php echo base_url('index.php/Mahasiswa'); ?>">Batal</a>
        </form>
    </center>
</body>

</html>