<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard</title>
</head>

<body>
    <center>
        <h2>Halo <?php echo $_SESSION['username']; ?></h2>

        <h3><a href="<?php echo base_url('index.php/Mahasiswa/logout'); ?>">LOGOUT</a></h3>
        <br>
        <form action="<?php echo base_url('index.php/Mahasiswa/insert_mahasiswa'); ?>" method="POST">
            <table>
                <tr>
                    <td>Nama</td>
                    <td>:</td>
                    <td><input type="text" name="nama"></td>
                </tr>
                <tr>
                    <td>Nim</td>
                    <td>:</td>
                    <td><input type="text" name="nim"></td>
                </tr>
            </table>
            <input type="submit" value="Simpan">
        </form>

        <table border="1px">
            <tr>
                <td>No</td>
                <td>Nama</td>
                <td>Nim</td>
                <td>Aksi</td>
            </tr>
            <?php
            $no = 1;
            foreach ($data as $mhs) { ?>
                <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $mhs->nama; ?></td>
                    <td><?php echo $mhs->nim; ?></td>
                    <td>
                        <a href="<?php echo base_url('index.php/Mahasiswa/delete_mahasiswa?id=').$mhs->id;  ?>">Hapus Data</a>
                        <br><br>
                        <a href="<?php echo base_url('index.php/Mahasiswa/update_mahasiswa?id=').$mhs->id;  ?>">Edit Data</a>
                    </td>
                </tr>
            <?php
                $no++;
            } ?>
        </table>
    </center>
</body>

</html>